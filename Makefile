LOCAL_TEST_IMAGE=test_vol_dash_api


setup_postgres:
	docker pull postgres:10.2
	docker run -d --rm \
		-e POSTGRES_PASSWORD=password \
		-e POSTGRES_USER=blog \
		-e POSTGRES_DB=test_db \
		-p ""5432:5432"" \
		postgres:10.2

deploy:
	docker login -u villy393 -p "$DOCKER_HUB_PASSWORD"
	docker build . -t villy393/personal_website_api:master
	docker push villy393/personal_website_api:master