# personal_website_api

The api that provides the necessary data for https://villy.me.

## Runing the App locally

```bash
# In the root of the directory
docker-compose up -d

pip install -r requirements.txt # Recommended to run this in a virtual environment.

python3 app.py
```


## Running the Tests

```bash
docker-compose up -d

pip install -r requirements.txt # Recommended to run this in a virtual environment.

pytest
```

### Env variables
[DynaConf](https://github.com/rochacbruno/dynaconf) is used to read settings. This application stores default settings in `settings.yml`. However, if an environment variable is set with the same key as a setting in the settings file, the environment variable will have precedence.