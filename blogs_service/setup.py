from setuptools import find_packages, setup


def list_reqs(fname='requirements.txt'):
    with open(fname) as fd:
        return fd.read().splitlines()


setup(
    name='blogs',
    version='0.0.1',
    description='Provides blogs functionality',
    author='Wilhelm Van Der Walt',
    license='All rights reserved',
    packages=find_packages(),
    include_package_data=True,
    install_requires=list_reqs(),
    dependency_links=[]
)