import pytest
from blogs import (
    create_app, generate_db)
from blogs.config import config


@pytest.fixture(scope="session")
def app():
    settings_override = {
        'TESTING': True
    }
    test_config = {**config.config, **settings_override}
    app = create_app(app=None, config=test_config)
    app.app_context().push()
    return app


@pytest.fixture()
def test_client(app):
    return app.test_client()


@pytest.fixture(scope="session")
def test_db(app):
    db = generate_db(app=app)
    yield db
    db.drop_all()


@pytest.fixture
def db_session(test_db):
    connection = test_db.engine.connect()
    transaction = connection.begin()
    options = dict(bind=connection, binds={})
    session = test_db.create_scoped_session(options=options)
    test_db.session = session

    yield session

    transaction.rollback()
    connection.close()
    session.remove()
