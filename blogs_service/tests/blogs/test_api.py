import json
from datetime import datetime
from http import client

from blogs.models.blogs import Blogs


def test_get_blog(test_client, db_session):
    # Given
    blog = Blogs(text="asfd")
    db_session.add(blog)
    db_session.flush()

    # When
    get_blog_res = test_client.get('/v1/blogs/' + str(blog.id))
    get_blogs_res = test_client.get('/v1/blogs')

    # Then
    get_blogs_res_json = json.loads(get_blogs_res.data.decode())
    get_blog_res_json = json.loads(get_blog_res.data.decode())
    assert get_blogs_res.status_code == 200
    assert get_blog_res.status_code == 200
    assert get_blogs_res_json == [get_blog_res_json]


def test_limit(test_client, db_session):
    # Given
    blog1 = Blogs(text="blog1")
    db_session.add(blog1)
    blog2 = Blogs(text="blog2")
    db_session.add(blog2)
    db_session.flush()

    # When
    get_blogs_res = test_client.get('/v1/blogs', query_string={"limit": 1})

    # Then
    assert len(get_blogs_res.json) == 1


def test_create_blog(test_client, db_session):
    # Given
    blog = {'text': 'test'}

    # When
    post_blogs_res = test_client.post(
        '/v1/blogs', data=json.dumps(blog),
        headers={'Content-Type': 'application/json'}
    )

    # Then
    assert post_blogs_res.status_code == client.CREATED
    assert post_blogs_res.json['id'] is not None


def test_order_get_blogs(test_client, db_session):
    # Given
    new_text = 'new'
    old_blog = Blogs(**{'text': 'old', 'created_at': datetime(2000, 1, 1)})
    middle_blog = Blogs(
        **{'text': 'middle', 'created_at': datetime(2011, 1, 1)})
    new_blog = Blogs(**{'text': new_text, 'created_at': datetime(2018, 1, 1)})

    db_session.add(old_blog)
    db_session.flush()

    db_session.add(new_blog)
    db_session.flush()

    db_session.add(middle_blog)
    db_session.flush()

    # When
    get_blogs_res = test_client.get('/v1/blogs', query_string={"limit": 1})

    # Then
    assert get_blogs_res.json[0]['text'] == new_text
