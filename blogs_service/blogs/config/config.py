import os

import yaml

from . import helpers

helpers.setup_yaml_parser()

current_file = os.path.dirname(os.path.abspath(__file__))

with open(current_file + "/config.yml", 'r') as file:
    config = yaml.load(file)
