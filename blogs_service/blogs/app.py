from flask import Flask, Blueprint
from .models.database import db
from .config.config import config as app_config

blogs_blueprint = None


def create_app(*, app, config=None):
    global blogs_blueprint
    _app = app or Flask(__name__)
    _config = config or app_config
    blogs = Blueprint('blogs', __name__)
    _app.config = {**_app.config, **_config}
    # set global blueprint
    blogs_blueprint = blogs
    from . import controller  # noqa
    _app.register_blueprint(blogs)
    db.init_app(_app)
    return _app


def generate_db(*, app):
    db.init_app(app)
    with app.app_context():
        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block.
        _db = init_db(db=db)
    return _db


def init_db(*, db):
    from .models import blogs # noqa
    db.create_all()
    db.session.flush()
    return db
