from flask import request

from .app import blogs_blueprint, db
from .models.blogs import Blogs
from .http import http_response


@blogs_blueprint.route('/v1/blogs/<blog_id>', methods=['GET'])
def get_blog(blog_id):
    blog = Blogs.query.get(blog_id)
    return http_response(
        status=200, body=blog.serialize()
    )


@blogs_blueprint.route('/v1/blogs', methods=['GET'])
def get_blogs():
    limit = request.args.get('limit')
    blogs = Blogs.query.order_by(Blogs.created_at.desc()).limit(limit).all()
    return http_response(
        status=200, body=Blogs.serialize_list(blogs)
    )


@blogs_blueprint.route('/v1/blogs', methods=['POST'])
def create_blogs():
    blog = request.get_json()
    new_blog = Blogs(**blog)
    db.session.add(new_blog)
    db.session.flush()
    new_blog_json = new_blog.serialize()
    db.session.commit()
    return http_response(
        status=201, body=new_blog_json
    )
