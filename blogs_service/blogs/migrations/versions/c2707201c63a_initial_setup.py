"""Initial setup

Revision ID: c2707201c63a
Revises:
Create Date: 2018-05-19 14:19:24.143307

"""
import uuid

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c2707201c63a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'blogs',
        sa.Column('id', sa.String, primary_key=True, default=uuid.uuid4),
        sa.Column('text', sa.String, nullable=False),
    )


def downgrade():
    op.drop_table('blogs')
