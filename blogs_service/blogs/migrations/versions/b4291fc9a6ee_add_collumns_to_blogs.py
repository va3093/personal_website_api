"""Add collumns to blogs

Revision ID: b4291fc9a6ee
Revises: c2707201c63a
Create Date: 2018-05-28 18:05:10.966455

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b4291fc9a6ee'
down_revision = 'c2707201c63a'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('blogs', sa.Column('created_at', sa.DateTime))
    op.add_column('blogs', sa.Column('updated_at', sa.DateTime))


def downgrade():
    op.drop_column('blogs', 'created_at')
    op.drop_column('blogs', 'updated_at')
