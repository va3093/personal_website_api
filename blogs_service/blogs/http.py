from flask import Response

import json


def http_response(
        *, status: int, headers: dict=None, body: dict=None):
    return Response(
        response=json.dumps(body),
        status=status,
        headers=headers or {},
        content_type="application/json"
    )