from datetime import datetime
import uuid

from sqlalchemy.inspection import inspect


class Serializer(object):

    def serialize_attr(self, attribute):
        if isinstance(attribute, uuid.UUID):
            return str(attribute)
        if isinstance(attribute, datetime):
            return attribute.isoformat()
        return attribute

    def serialize(self):
        return {
            c: self.serialize_attr(getattr(self, c))
            for c in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]