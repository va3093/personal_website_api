import uuid
import datetime

from .database import db
from .serialization import Serializer


class Blogs(db.Model, Serializer):
    id = db.Column(db.String, primary_key=True, default=uuid.uuid4)
    text = db.Column(db.Text(), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
