from flask import Flask
from blogs.app import (
    create_app as blog_create_app)

app = Flask(__name__)


app = blog_create_app(app=app)
