#!/bin/bash
# Allows the scripts to be found indepenetly from the cwd
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

IS_DEBUG=${DEBUG:-false}

# Silence FSADeprecationWarning: SQLALCHEMY_TRACK_MODIFICATIONS warnging:
# adds significant overhead and will be disabled by default in the future
SQLALCHEMY_TRACK_MODIFICATIONS=false


# Perform Migrations

echo -e "\nSleeping to wait for dbs to come up\n"
sleep 2

# Blogs
echo -e "\nRunning Blogs migrations\n"
PYTHONPATH=. alembic -c $(python3 $SCRIPTPATH/alembic_config_getter.py blogs) upgrade head

# Run App
if [ "$IS_DEBUG" = true ]; then
    echo -e "\nDEBUG env var is set so running Flask App\n"
    export FLASK_DEBUG=true
    export FLASK_APP=$SCRIPTPATH/app.py
    flask run --port=7574 --host=0.0.0.0
else
    echo -e "\nRunning UWSGI apps\n"
    uwsgi --ini $SCRIPTPATH/uwsgi.ini
fi