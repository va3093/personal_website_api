import os
import sys
import importlib

service = sys.argv[1]

module = importlib.import_module(service)

alembic_config_path = os.path.join(os.path.dirname(module.__file__),
                                   'config/alembic.ini')

print(alembic_config_path)
