import json

import requests
from http import client

from . import constants


def test_save_and_get_blog():
    # Given
    blog = {"text": "test"}

    # When
    post_res = requests.post(
        constants.BLOGS_V1_URL, data=json.dumps(blog),
        headers={'Content-Type': 'application/json'})
    get_blog_res = requests.get(
        constants.BLOGS_V1_URL + "/" + post_res.json()['id'])

    get_blogs_res = requests.get(constants.BLOGS_V1_URL, params={"limit": 1})

    # Then
    assert post_res.status_code == client.CREATED

    assert get_blog_res.json()['text'] == blog['text']

    assert get_blogs_res.json() == [get_blog_res.json()]
