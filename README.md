# personal_website_api

The api that provides the necessary data for https://villy.me.

This repository is a micro monolith and has the fundamental goal of being highly modularised but able to easily add cross function features.

## Micro-monolith

This project tries merge the benefits of micro services with the benefits of single code bases. These design goals are.

- Modularity -> applications seperated by function and technically enforced
- Composable -> Easy to switch on application with another with the same api
- Code re-use -> Simple sharing of code among applications
- Development scale -> Allow multiple teams to work on the repository without worrying about complex merge coflics
- Inegration testing -> Easy to test cross application functions

To achieve the above Flask blueprints and python packaging is used. Each application should conform to the following interface.
- Should contain a `create_app` function in a module called `app`. This function should accept a `Flask` app object and return a `Flask` app object. This is called by the serving application
- If the app has database migrations the `alembic.ini` file must be kept in `service/config/alembic.ini`. This must be included in the package by using the `MANIFEST.ini` file.
- Must include a `setup.py` file that can be used to install the service as a package by the serving application.


The serving application first runs all the services migrations and then imports all the packages and calls their `create_app` function. It then serves the application as if it is one big monolith.

## Running the App locally

```bash
# In the root of the directory
docker-compose up -d
```
This will run all dependencies and build the application.

If are doing local develpment you may want to only run the dependencies in docker but run the application locally. This will make flasks auto-reload feature easier to manage and you are not likely to enter into docker cache hell.

```bash
# In the root of the directory
docker-compose up -d

docker-compose stop personal_website_api

# Run the app locally
DEBUG=true personal_website_api/run.sh
```


## Running the Tests

All services have there own tests. to run their tests cd into the service, install dependencies and run tests. If this does not work, then consult the services readme file for instructions.

The `personal_website_api` directory contains functional tests. They are different in that they hit a running application instance. So you need to be [running the service locally](#running-the-app-locally) before running these tests.
